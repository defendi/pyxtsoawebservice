#*******************************************************************#
# Adicionais de PEFIN, CREDNET,                                     #
# __  _______ _                                                     #
# \ \/ /_   _(_) __ _  ___ _ __       © 2019 Alexandre Defendi      #
#  \  /  | | | |/ _` |/ _ \ '__|       Created on Nov 01, 2019      #
#  /  \  | | | | (_| |  __/ |       alexandre_defendi@hotmail.com   #
# /_/\_\ |_| |_|\__, |\___|_|                                       #
#               |___/                                               #
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html) #
#*******************************************************************#

ADD_NENHUM = 'Nenhum'
ADD_QSOCIOS = 'QuadroDeSocios'
ADD_PARTICIPA = 'Participacoes'
ADD_RSCORING = 'RiskScoring'
ADD_LCREDIT = 'LimiteCredito'
ADD_CLRISCO = 'ClassificacaoRiscoCredito'
ADD_LIST = [ADD_NENHUM,ADD_QSOCIOS,ADD_PARTICIPA,ADD_RSCORING,ADD_LCREDIT,ADD_CLRISCO]

DEVEDOR_PRINCIPAL = 'Principal'
DEVEDOR_COOBRIGADO = 'CoObrigado'

COMUNICADO_FAC = 'FAC'
COMUNICADO_BOLETO = 'BoletoBancario'
COMUNICADO_CPUBLICA = 'ContasPublicas'
COMUNICADO_AR = 'AR'